package com.uks.sisserveruks.service;

import com.uks.sisserveruks.dto.PenangananPasien;
import com.uks.sisserveruks.model.StatusPeriksa;

import java.util.List;

// untuk membuat method atau fungsi
public interface StatusPeriksaService {
    //    method post
    StatusPeriksa add(PenangananPasien penangananPasien);

    //    method get by id
    StatusPeriksa getById(Long id);

    //    method get all
    List<StatusPeriksa> getAll();

    //    method get all by pasien id
    List<StatusPeriksa> getByPasien(Long pasienId);
}
