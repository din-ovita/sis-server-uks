package com.uks.sisserveruks.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

// membuat response
public class ResponseHelper {
//    response ketika sukses
    public static <D> CommonResponse<D> okey(D data) {
        CommonResponse<D> response = new CommonResponse<D>();
        response.setMessage("Success!");
        response.setStatus("200");
        response.setData(data);
        return response;
    }

//    response ketika error
    public static <D> org.springframework.http.ResponseEntity<CommonResponse<D>> error(String error, HttpStatus httpStatus) {
        CommonResponse<D> response = new CommonResponse<>();
        response.setStatus(String.valueOf(httpStatus.value()));
        response.setMessage(httpStatus.name());
        response.setData((D) error);
        return new ResponseEntity<>(response, httpStatus);
    }
}
