package com.uks.sisserveruks.service;


import com.uks.sisserveruks.dto.PasienDto;
import com.uks.sisserveruks.dto.StatusPeriksa;
import com.uks.sisserveruks.model.Pasien;

import java.util.Date;
import java.util.List;
import java.util.Map;

// untuk membuat method atau fungsi
public interface PasienService {
    //    method post
    Pasien addPasien(PasienDto pasien);

    //    method get by id
    Pasien getById(Long id);

    //    method put
    Pasien putPasien(PasienDto pasien, Long id);

    //    method get all
    List<Pasien> allPasien();

    //    method delete
    Map<String, Boolean> deletePasien(Long id);

    //    method get all by status
    List<Pasien> findStatus(String status);

    //    method put pasien setelah ditangani
    Pasien putPeriksa(Long id, StatusPeriksa pasien);

//    //    method put pasien setelah ditangani
//    Map<String, Object> getBetween(Date start, Date endDate);

}
