package com.uks.sisserveruks.model;

import javax.persistence.*;

// membuat table diagnosa
@Entity
// nama table
@Table(name = "diagnosa")
public class Diagnosa {
    //    column table
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama_diagnosa")
    private String namaDiagnosa;

    //    constructor kosong, untuk mempermudah pemanggilan class
    public Diagnosa() {
    }

    //    getter and setter, untuk mempermudah pemanggilan setiap column
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaDiagnosa() {
        return namaDiagnosa;
    }

    public void setNamaDiagnosa(String namaDiagnosa) {
        this.namaDiagnosa = namaDiagnosa;
    }
}
