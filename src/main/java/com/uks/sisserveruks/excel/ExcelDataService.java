package com.uks.sisserveruks.excel;

import com.uks.sisserveruks.exception.NotFoundException;
import com.uks.sisserveruks.model.Data;
import com.uks.sisserveruks.model.Pasien;
import com.uks.sisserveruks.model.Role;
import com.uks.sisserveruks.model.StatusPeriksa;
import com.uks.sisserveruks.repository.DataRepo;
import com.uks.sisserveruks.repository.PasienRepo;
import com.uks.sisserveruks.repository.StatusPeriksaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Service
public class ExcelDataService {
    @Autowired
    DataRepo dataRepo;

    @Autowired
    PasienRepo pasienRepo;

    @Autowired
    StatusPeriksaRepo statusPeriksaRepo;

//    method get all by status, menjadi excel
    public ByteArrayInputStream load(String status) {
        List<Data> data = dataRepo.findByStatus(Role.valueOf(status));
        ByteArrayInputStream in = ExcelDataHelper.dataToExcel(data);
        return in;
    }

//    method get by id for download data periksa
     @Transactional(readOnly = true)
    public ByteArrayInputStream download(Long id) {
        Pasien periksa = pasienRepo.findById(id).orElseThrow(() -> new NotFoundException("Id not found"));
        StatusPeriksa penangananPeriksa = statusPeriksaRepo.findByPasienId(periksa.getId());
        ByteArrayInputStream in = ExcelDataHelper.download(periksa, penangananPeriksa);
        return in;
    }

//    method upload
    public void saveData(MultipartFile file) {
        try {
            List<Data> dataList = ExcelDataHelper.exceltoData(file.getInputStream());
            dataRepo.saveAll(dataList);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }

}
