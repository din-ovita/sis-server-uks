package com.uks.sisserveruks.controller;

import com.uks.sisserveruks.model.Tindakan;
import com.uks.sisserveruks.response.CommonResponse;
import com.uks.sisserveruks.response.ResponseHelper;
import com.uks.sisserveruks.service.TindakanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

// controller untuk menjalankan sistem method
@RestController
// alamat controller
@RequestMapping("/tindakan")
public class TindakanController {
    @Autowired
    TindakanService tindakanService;

    //    method post
    @PostMapping
    public CommonResponse<Tindakan> addTindakan(@RequestBody Tindakan tindakan) {
        return ResponseHelper.okey(tindakanService.addTindakan(tindakan));
    }

    //    method get by id
    @GetMapping("/{id}")
    public CommonResponse<Tindakan> getId(@PathVariable("id") Long id) {
        return ResponseHelper.okey(tindakanService.getId(id));
    }

    //    method get all
    @GetMapping("/all-tindakan")
    public CommonResponse<List<Tindakan>> getAllTindakan() {
        return ResponseHelper.okey(tindakanService.getAllTindakan());
    }

    //    method put
    @PutMapping("/{id}")
    public CommonResponse<Tindakan> putTindakan(@PathVariable("id") Long id, @RequestBody Tindakan tindakan) {
        return ResponseHelper.okey(tindakanService.putTindakan(id, tindakan));
    }

    //    method delete
    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> delete(@PathVariable("id") Long id) {
        return ResponseHelper.okey(tindakanService.delete(id));
    }
}
