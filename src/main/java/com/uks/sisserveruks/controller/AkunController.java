package com.uks.sisserveruks.controller;

import com.uks.sisserveruks.dto.*;
import com.uks.sisserveruks.model.Akun;
import com.uks.sisserveruks.response.CommonResponse;
import com.uks.sisserveruks.response.ResponseHelper;
import com.uks.sisserveruks.service.AkunService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

// controller untuk menjalankan sistem method
@RestController
// alamat controller
@RequestMapping("/akun")
public class AkunController {
    @Autowired
    AkunService akunService;

    @Autowired
    ModelMapper modelMapper;

//    method register
    @PostMapping("/register")
    public CommonResponse<Akun> register(@RequestBody Register akun) {
        return ResponseHelper.okey(akunService.register(modelMapper.map(akun, Akun.class)));
    }

//    method login
    @PostMapping("/login")
    public CommonResponse<Map<String, Object>> login(@RequestBody Login login) {
        return ResponseHelper.okey(akunService.login(login));
    }

//    method get by id
    @GetMapping("/{id}")
    public CommonResponse<Akun> getById(@PathVariable("id") Long id) {
        return ResponseHelper.okey(akunService.getById(id));
    }

//    method get all
    @GetMapping("/all-akun")
    public CommonResponse<List<Akun>> getAll() {
        return ResponseHelper.okey(akunService.getAll());
    }

//    method put
    @PutMapping(path = "/update/{id}", consumes = "multipart/form-data")
    public CommonResponse<Akun> update(@PathVariable("id") Long id, Update akun, @RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.okey(akunService.update(id, modelMapper.map(akun, Akun.class), multipartFile));
    }

//    method put password
    @PutMapping(path = "/password/{id}")
    public CommonResponse<Akun> updatePassword(@PathVariable("id") Long id, @RequestBody Password password) {
        return ResponseHelper.okey(akunService.updatePassword(id, modelMapper.map(password, Akun.class)));
    }

//    method delete
    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> deleteAkun(@PathVariable("id") Long id) {
        return ResponseHelper.okey(akunService.deleteAkun(id));
    }
}
