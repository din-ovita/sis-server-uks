package com.uks.sisserveruks.model;

import javax.persistence.*;

// membuat table penanganan pertama
@Entity
// nama table
@Table(name = "penanganan")
public class Penanganan {
    //    column table
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama_penanganan")
    private String namaPenanganan;

    //    constructor kosong, untuk mempermudah pemanggilan class
    public Penanganan() {
    }

    //    getter and setter, untuk mempermudah pemanggilan setiap column
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaPenanganan() {
        return namaPenanganan;
    }

    public void setNamaPenanganan(String namaPenanganan) {
        this.namaPenanganan = namaPenanganan;
    }
}
