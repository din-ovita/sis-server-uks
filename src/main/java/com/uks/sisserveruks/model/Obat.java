package com.uks.sisserveruks.model;

import javax.persistence.*;

// membuat table obat
@Entity
// nama table
@Table(name = "obat")
public class Obat {
    //    column table
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama_obat")
    private String namaObat;

    @Column(name = "stok")
    private Long stok;

    //    constructor kosong, untuk mempermudah pemanggilan class
    public Obat() {
    }

    //    getter and setter, untuk mempermudah pemanggilan setiap column
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaObat() {
        return namaObat;
    }

    public void setNamaObat(String namaObat) {
        this.namaObat = namaObat;
    }

    public Long getStok() {
        return stok;
    }

    public void setStok(Long stok) {
        this.stok = stok;
    }
}
