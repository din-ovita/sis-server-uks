package com.uks.sisserveruks.impl;

import com.uks.sisserveruks.exception.NotFoundException;
import com.uks.sisserveruks.model.Tindakan;
import com.uks.sisserveruks.repository.TindakanRepo;
import com.uks.sisserveruks.service.TindakanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// membuat sistem dari method di file service (implementasi method dari file service)
@Service
public class TindakanImpl implements TindakanService {
    @Autowired
    TindakanRepo tindakanRepo;

    //    sistem post
    @Override
    public Tindakan addTindakan(Tindakan tindakan) {
        return tindakanRepo.save(tindakan);
    }

    //    sistem get by id
    @Override
    public Tindakan getId(Long id) {
        return tindakanRepo.findById(id).orElseThrow(() -> new NotFoundException("Id not found!"));
    }

    //    sistem get all
    @Override
    public List<Tindakan> getAllTindakan() {
        return tindakanRepo.findAll();
    }

    //    sistem update atau put
    @Override
    public Tindakan putTindakan(Long id, Tindakan tindakan) {
        Tindakan tindakans = tindakanRepo.findById(id).orElseThrow(() -> new NotFoundException("Id not found!"));
        tindakans.setNamaTindakan(tindakan.getNamaTindakan());
        return tindakanRepo.save(tindakans);
    }

    //    sistem delete
    @Override
    public Map<String, Boolean> delete(Long id) {
        try {
            tindakanRepo.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Deleted!", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id not found!");
        }
    }
}
