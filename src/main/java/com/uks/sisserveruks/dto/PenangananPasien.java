package com.uks.sisserveruks.dto;

// membuat dto penanganan
public class PenangananPasien {
    private Long pasienId;

    private Long diagnosaId;

    private Long penangananId;

    private Long tindakanId;

    private Long obatId;

    public PenangananPasien() {
    }

    public Long getPasienId() {
        return pasienId;
    }

    public void setPasienId(Long pasienId) {
        this.pasienId = pasienId;
    }

    public Long getDiagnosaId() {
        return diagnosaId;
    }

    public void setDiagnosaId(Long diagnosaId) {
        this.diagnosaId = diagnosaId;
    }

    public Long getPenangananId() {
        return penangananId;
    }

    public void setPenangananId(Long penangananId) {
        this.penangananId = penangananId;
    }

    public Long getTindakanId() {
        return tindakanId;
    }

    public void setTindakanId(Long tindakanId) {
        this.tindakanId = tindakanId;
    }

    public Long getObatId() {
        return obatId;
    }

    public void setObatId(Long obatId) {
        this.obatId = obatId;
    }
}
