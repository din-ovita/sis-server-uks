package com.uks.sisserveruks.dto;

// membuat dto update akun
public class Update {
    private String username;

    private String email;

    public Update() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
