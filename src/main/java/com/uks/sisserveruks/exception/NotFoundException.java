package com.uks.sisserveruks.exception;

// membuat kondisi error not found (tidak ditemukan)
public class NotFoundException extends RuntimeException{
    public NotFoundException(String message) {super(message);}
}
