package com.uks.sisserveruks.model;

import javax.persistence.*;

// membuat table status periksa
@Entity
// nama table
@Table(name = "status_periksa")
public class StatusPeriksa {
    //    column table
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "pasien_id")
    private Pasien pasienId;

    @ManyToOne
    @JoinColumn(name = "diagnosa_id")
    private Diagnosa diagnosaId;

    @ManyToOne
    @JoinColumn(name = "penanganan_id")
    private Penanganan penangananId;

    @ManyToOne
    @JoinColumn(name = "tindakan_id")
    private Tindakan tindakanId;

    @ManyToOne
    @JoinColumn(name = "obat_id")
    private Obat obatId;

    //    constructor kosong, untuk mempermudah pemanggilan class
    public StatusPeriksa() {
    }

    //    getter and setter, untuk mempermudah pemanggilan setiap column
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Pasien getPasienId() {
        return pasienId;
    }

    public void setPasienId(Pasien pasienId) {
        this.pasienId = pasienId;
    }

    public Diagnosa getDiagnosaId() {
        return diagnosaId;
    }

    public void setDiagnosaId(Diagnosa diagnosaId) {
        this.diagnosaId = diagnosaId;
    }

    public Penanganan getPenangananId() {
        return penangananId;
    }

    public void setPenangananId(Penanganan penangananId) {
        this.penangananId = penangananId;
    }

    public Tindakan getTindakanId() {
        return tindakanId;
    }

    public void setTindakanId(Tindakan tindakanId) {
        this.tindakanId = tindakanId;
    }

    public Obat getObatId() {
        return obatId;
    }

    public void setObatId(Obat obatId) {
        this.obatId = obatId;
    }
}
