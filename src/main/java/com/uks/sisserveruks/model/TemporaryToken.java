package com.uks.sisserveruks.model;

import javax.persistence.*;
import java.util.Date;

// membuat token
@Table
// nama table
@Entity(name = "token")
public class TemporaryToken {
    //    column table
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String token;

    @Column(name = "expired_date")
    private Date expiredDate;

    @Column(name = "akun_id")
    private long akunId;

    //    constructor kosong, untuk mempermudah pemanggilan class
    public TemporaryToken() {
    }

    //    getter and setter, untuk mempermudah pemanggilan setiap column
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public long getAkunId() {
        return akunId;
    }

    public void setAkunId(long akunId) {
        this.akunId = akunId;
    }
}
