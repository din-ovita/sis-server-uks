package com.uks.sisserveruks.repository;

import com.uks.sisserveruks.model.Obat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

// membuat repository, untuk membuat query sqlyog
@Repository
public interface ObatRepo extends JpaRepository<Obat, Long> {
}
