package com.uks.sisserveruks.service;

import com.uks.sisserveruks.model.Obat;

import java.util.List;
import java.util.Map;

// untuk membuat method atau fungsi
public interface ObatService {
    //    method post
    Obat addObat(Obat obat);

    //    method get by id
    Obat getById(Long id);

    //    method get all
    List<Obat> getAllObat();

    //    method put
    Obat updateObat(Long id, Obat obat);

    //    method delete
    Map<String, Boolean> deleteObat(Long id);
}
