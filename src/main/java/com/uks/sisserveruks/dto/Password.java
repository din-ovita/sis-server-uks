package com.uks.sisserveruks.dto;

// membuat dto password
public class Password {
    String password;

    public Password() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
