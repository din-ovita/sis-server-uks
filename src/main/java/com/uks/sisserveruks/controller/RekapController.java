package com.uks.sisserveruks.controller;

import com.uks.sisserveruks.impl.PasienImpl;
import com.uks.sisserveruks.response.CommonResponse;
import com.uks.sisserveruks.response.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Map;

// controller untuk menjalankan sistem method
@RestController
// alamat controller
@RequestMapping("/rekap-data")
public class RekapController {
    @Autowired
    PasienImpl pasienImpl;

//    method get data by filter tgl
    @GetMapping
    public CommonResponse<Map<String, Object>> getRekapData(@RequestParam("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime startDate, @RequestParam("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime endDate) {
        return ResponseHelper.okey(pasienImpl.getBetween(startDate, endDate));
    }

}
