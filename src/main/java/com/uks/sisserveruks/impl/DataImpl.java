package com.uks.sisserveruks.impl;

import com.uks.sisserveruks.exception.NotFoundException;
import com.uks.sisserveruks.model.Data;
import com.uks.sisserveruks.model.Role;
import com.uks.sisserveruks.repository.DataRepo;
import com.uks.sisserveruks.service.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// membuat sistem dari method di file service (implementasi method dari file service)
@Service
public class DataImpl implements DataService {
    @Autowired
    DataRepo dataRepo;

    //    sistem post
    @Override
    public Data addData(Data data) {
        if (data.getStatus().name().equals("siswa")) {
            data.setStatus(Role.siswa);
        } else if (data.getStatus().name().equals("guru")) {
            data.setStatus(Role.guru);
        } else if (data.getStatus().name().equals("karyawan")) {
            data.setStatus(Role.karyawan);
        }
        return dataRepo.save(data);
    }

    //    sistem get by id
    @Override
    public Data getById(Long id) {
        return dataRepo.findById(id).orElseThrow(() -> new NotFoundException("Id not found!"));
    }

    //    sistem get all
    @Override
    public List<Data> getAllData() {
        return dataRepo.findAll();
    }

    //    sistem update atau put
    @Override
    public Data putData(Long id, Data data) {
        Data data1 = dataRepo.findById(id).orElseThrow(() -> new NotFoundException("Id not found!"));
        if (data.getStatus().name().equals("guru")) {
            data1.setStatus(Role.guru);
        } else if (data.getStatus().name().equals("siswa")) {
            data1.setStatus(Role.siswa);
        } else if (data.getStatus().name().equals("karyawan")) {
            data1.setStatus(Role.karyawan);
        }
        data1.setAlamat(data.getAlamat());
        data1.setNama(data.getNama());
        data1.setJabatan(data.getJabatan());
        data1.setTempatLahir(data.getTempatLahir());
        data1.setTglLahir(data.getTglLahir());
        return dataRepo.save(data1);
    }

    //    sistem delete
    @Override
    public Map<String, Boolean> delete(Long id) {
        try {
            dataRepo.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Deleted!", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id not found!");
        }
    }

    //    sistem get all by status
    @Override
    public List<Data> findStatus(String status) {
        return dataRepo.findByStatus(Role.valueOf(status));
    }
}
