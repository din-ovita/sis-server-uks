package com.uks.sisserveruks.excel;

import com.uks.sisserveruks.model.Data;
import com.uks.sisserveruks.model.Pasien;
import com.uks.sisserveruks.model.Role;
import com.uks.sisserveruks.model.StatusPeriksa;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class ExcelDataHelper {
//    type file
    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

//    header in excel
    static String[] HEADER_DATA = {"nama", "jabatan", "status", "tempat_lahir", "tgl_lahir", "alamat"};

//    header in excel
    static String[] HEADER_PERIKSA = {"nama_pasien", "status_pasien", "jabatan", "keluhan_pasien", "tgl_periksa", "penanganan", "penanganan_pertama", "diagnosa", "obat", "tindakan"};

//    name sheet in excel
    static String SHEET = "Sheet1";

//    validasi type file
    public static boolean hasExcelFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }

//    sistem data menjadi excel
    public static ByteArrayInputStream dataToExcel(List<Data> data) {
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet(SHEET);

            Row headerRow = sheet.createRow(0);

            for (int col = 0; col < HEADER_DATA.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(HEADER_DATA[col]);
            }

            int rowIdx = 1;
            for (Data data1 : data) {
                Row row = sheet.createRow(rowIdx++);

                row.createCell(0).setCellValue(data1.getNama());
                row.createCell(1).setCellValue(data1.getJabatan());
                row.createCell(2).setCellValue(data1.getStatus().name());
                row.createCell(3).setCellValue(data1.getTempatLahir());
                row.createCell(4).setCellValue(data1.getTglLahir());
                row.createCell(5).setCellValue(data1.getAlamat());
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
        }
    }

//    sistem get data periksa
    @Transactional(readOnly = true)
    public static ByteArrayInputStream download(Pasien periksaList, StatusPeriksa penangananPeriksa) {
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet(SHEET);

            Row headerRow = sheet.createRow(0);

            for (int col = 0; col < HEADER_PERIKSA.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(HEADER_PERIKSA[col]);
            }

            int rowIdx = 1;
            Row row = sheet.createRow(rowIdx);

            row.createCell(0).setCellValue(periksaList.getData().getNama());
            row.createCell(1).setCellValue(periksaList.getStatus().name());
            row.createCell(2).setCellValue(periksaList.getData().getJabatan());
            row.createCell(3).setCellValue(periksaList.getKeluhan());
            row.createCell(4).setCellValue(periksaList.getTglPeriksa());
            row.createCell(5).setCellValue(periksaList.getPenanganan());
            row.createCell(6).setCellValue(penangananPeriksa.getPenangananId().getNamaPenanganan());
            row.createCell(7).setCellValue(penangananPeriksa.getDiagnosaId().getNamaDiagnosa());
            row.createCell(8).setCellValue(penangananPeriksa.getObatId().getNamaObat());
            row.createCell(9).setCellValue(penangananPeriksa.getTindakanId().getNamaTindakan());

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
        }
    }

//    sistem excel menjadi data
    public static List<Data> exceltoData(InputStream is) {

        try {
            Workbook workbook = new XSSFWorkbook(is);

            Sheet sheet = workbook.getSheet(SHEET);
            Iterator<Row> rows = sheet.iterator();

            List<Data> dataList = new ArrayList<Data>();
            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();

                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();

                Data data = new Data();

                int cellIdx = 0;
                while (cellsInRow.hasNext()) {
                    Cell currentCell = cellsInRow.next();

                    switch (cellIdx) {
                        case 0:
                            data.setId((long) currentCell.getNumericCellValue());
                            break;
                        case 1:
                            data.setNama(currentCell.getStringCellValue());
                            break;
                        case 2:
                            data.setJabatan(currentCell.getStringCellValue());
                            break;
                        case 3:
                            data.setStatus(Role.valueOf(currentCell.getStringCellValue()));
                            break;
                        case 4:
                            data.setTempatLahir(currentCell.getStringCellValue());
                            break;
                        case 5:
                            data.setTglLahir(String.valueOf(currentCell.getDateCellValue()));
                            break;
                        case 6:
                            data.setAlamat(currentCell.getStringCellValue());
                            break;
                        default:
                            break;

                    }
                    cellIdx++;

                }
                    dataList.add(data);
            }
            workbook.close();
            return dataList;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
        }
    }
}
