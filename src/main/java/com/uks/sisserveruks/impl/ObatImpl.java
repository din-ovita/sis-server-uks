package com.uks.sisserveruks.impl;

import com.uks.sisserveruks.exception.NotFoundException;
import com.uks.sisserveruks.model.Obat;
import com.uks.sisserveruks.repository.ObatRepo;
import com.uks.sisserveruks.service.ObatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// membuat sistem dari method di file service (implementasi method dari file service)
@Service
public class ObatImpl implements ObatService {
    @Autowired
    ObatRepo obatRepo;

    //    sistem post
    @Override
    public Obat addObat(Obat obat) {
        return obatRepo.save(obat);
    }

    //    sistem get by id
    @Override
    public Obat getById(Long id) {
        return obatRepo.findById(id).orElseThrow(() -> new NotFoundException ("Id not found"));
    }

    //    sistem get all
    @Override
    public List<Obat> getAllObat() {
        return obatRepo.findAll();
    }

    //    sistem update atau put
    @Override
    public Obat updateObat(Long id, Obat obat) {
        Obat obats = obatRepo.findById(id).orElseThrow(() -> new NotFoundException("Id not found!"));
        obats.setNamaObat(obat.getNamaObat());
        obats.setStok(obat.getStok());
        return obatRepo.save(obats);
    }

    //    sistem delete
    @Override
    public Map<String, Boolean> deleteObat(Long id) {
        try {
            obatRepo.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Deleted!", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id not found!");
        }
    }
}
