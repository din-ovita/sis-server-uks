package com.uks.sisserveruks.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

// membuat table data pasien
@Entity
// nama table
@Table(name = "pasien")
public class Pasien {
    //    column table
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "keluhan")
    private String keluhan;

    @ManyToOne
    @JoinColumn(name = "data_id")
    private Data data;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @CreationTimestamp
    @Column(name = "tgl_periksa")
    private LocalDateTime tglPeriksa;

    @Column(name = "penanganan")
    private String penanganan;

    @Column(name = "status")
    private Role status;

    //    constructor kosong, untuk mempermudah pemanggilan class
    public Pasien() {
    }

    //    getter and setter, untuk mempermudah pemanggilan setiap column
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKeluhan() {
        return keluhan;
    }

    public void setKeluhan(String keluhan) {
        this.keluhan = keluhan;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public LocalDateTime getTglPeriksa() {
        return tglPeriksa;
    }

    public void setTglPeriksa(LocalDateTime tglPeriksa) {
        this.tglPeriksa = tglPeriksa;
    }

    public String getPenanganan() {
        return penanganan;
    }

    public void setPenanganan(String penanganan) {
        this.penanganan = penanganan;
    }

    public Role getStatus() {
        return status;
    }

    public void setStatus(Role status) {
        this.status = status;
    }

}
