package com.uks.sisserveruks.repository;

import com.uks.sisserveruks.model.Data;
import com.uks.sisserveruks.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

// membuat repository, untuk membuat query sqlyog
@Repository
public interface DataRepo extends JpaRepository<Data, Long> {
//    query get all by status
    List<Data> findByStatus(Role status);
}
