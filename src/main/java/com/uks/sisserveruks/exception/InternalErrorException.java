package com.uks.sisserveruks.exception;

// membuat kondisi internal error (status 500)
public class InternalErrorException extends RuntimeException{
    public InternalErrorException(String message) {super(message);}
}
