package com.uks.sisserveruks.repository;

import com.uks.sisserveruks.model.TemporaryToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

// membuat repository, untuk membuat query sqlyog
@Repository
public interface TokenRepo extends JpaRepository<TemporaryToken, Long> {
    Optional<TemporaryToken> findByToken(String token);
    Optional<TemporaryToken> findByAkunId(long akunId);

}
