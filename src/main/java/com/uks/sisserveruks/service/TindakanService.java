package com.uks.sisserveruks.service;

import com.uks.sisserveruks.model.Tindakan;

import java.util.List;
import java.util.Map;

// untuk membuat method atau fungsi
public interface TindakanService {
//    method post
    Tindakan addTindakan(Tindakan tindakan);

//    method get by id
    Tindakan getId(Long id);

//    method get all
    List<Tindakan> getAllTindakan();

//    method put
    Tindakan putTindakan(Long id, Tindakan tindakan);

//    method delete
    Map<String, Boolean> delete(Long id);

}
