package com.uks.sisserveruks.model;

import javax.persistence.*;

// membuat table tindakan
@Entity
// nama table
@Table(name = "tindakan")
public class Tindakan {
    //    column table
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama_tindakan")
    private String namaTindakan;

    //    constructor kosong, untuk mempermudah pemanggilan class
    public Tindakan() {
    }

    //    getter and setter, untuk mempermudah pemanggilan setiap column
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaTindakan() {
        return namaTindakan;
    }

    public void setNamaTindakan(String namaTindakan) {
        this.namaTindakan = namaTindakan;
    }
}
