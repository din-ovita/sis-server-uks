package com.uks.sisserveruks.impl;

import com.uks.sisserveruks.exception.NotFoundException;
import com.uks.sisserveruks.model.Diagnosa;
import com.uks.sisserveruks.repository.DiagnosaRepo;
import com.uks.sisserveruks.service.DiagnosaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// membuat sistem dari method di file service (implementasi method dari file service)
@Service
public class DiagnosaImpl implements DiagnosaService {
    @Autowired
    DiagnosaRepo diagnosaRepo;

    //    sistem post
    @Override
    public Diagnosa addDiagnosa(Diagnosa diagnosa) {
        return diagnosaRepo.save(diagnosa);
    }

    //    sistem get by id
    @Override
    public Diagnosa getById(Long id) {
        return diagnosaRepo.findById(id).orElseThrow(() -> new NotFoundException("Id not found!"));
    }

    //    sistem get all
    @Override
    public List<Diagnosa> getAllDiagnosa() {
        return diagnosaRepo.findAll();
    }

    //    sistem update atau put
    @Override
    public Diagnosa putDiagnosa(Long id, Diagnosa diagnosa) {
        Diagnosa diagnosas = diagnosaRepo.findById(id).orElseThrow(() -> new NotFoundException("Id not found!"));
        diagnosas.setNamaDiagnosa(diagnosa.getNamaDiagnosa());
        return diagnosaRepo.save(diagnosas);
    }

    //    sistem delete
    @Override
    public Map<String, Boolean> delete(Long id) {
        try {
            diagnosaRepo.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Deleted!", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id not found!");
        }
    }
}
