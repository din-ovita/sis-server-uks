package com.uks.sisserveruks.repository;

import com.uks.sisserveruks.model.StatusPeriksa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

// membuat repository, untuk membuat query sqlyog
@Repository
public interface StatusPeriksaRepo extends JpaRepository<StatusPeriksa, Long> {
//    query get all by pasein id
    @Query(value = "SELECT * FROM status_periksa  WHERE pasien_id = :pasienId", nativeQuery = true)
    List<StatusPeriksa> findByPasien(Long pasienId);

    @Query(value = "SELECT * FROM status_periksa  WHERE pasien_id = :pasienId", nativeQuery = true)
    StatusPeriksa findByPasienId(Long pasienId);

}

