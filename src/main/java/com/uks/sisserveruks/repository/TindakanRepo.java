package com.uks.sisserveruks.repository;

import com.uks.sisserveruks.model.Tindakan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

// membuat repository, untuk membuat query sqlyog
@Repository
public interface TindakanRepo extends JpaRepository<Tindakan, Long> {
}
