package com.uks.sisserveruks.controller;

import com.uks.sisserveruks.model.Penanganan;
import com.uks.sisserveruks.response.CommonResponse;
import com.uks.sisserveruks.response.ResponseHelper;
import com.uks.sisserveruks.service.PenangananService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

// controller untuk menjalankan sistem method
@RestController
// alamat controller
@RequestMapping("/penanganan")
public class PenangananController {
    @Autowired
    PenangananService penangananService;

    //    method post
    @PostMapping
    public CommonResponse<Penanganan> addPenanganan(@RequestBody Penanganan penanganan) {
        return ResponseHelper.okey(penangananService.addPenanganan(penanganan));
    }

    //    method get by id
    @GetMapping("/{id}")
    public CommonResponse<Penanganan> getId(@PathVariable("id") Long id) {
        return ResponseHelper.okey(penangananService.getId(id));
    }

    //    method get all
    @GetMapping("/all-penanganan")
    public CommonResponse<List<Penanganan>> getAllPenanganan() {
        return ResponseHelper.okey(penangananService.getAllPenanganan());
    }

    //    method put
    @PutMapping("/{id}")
    public CommonResponse<Penanganan> putPenanganan(@PathVariable("id") Long id, @RequestBody Penanganan penanganan) {
        return ResponseHelper.okey(penangananService.putPenanganan(id, penanganan));
    }

    //    method delete
    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> delete(@PathVariable("id") Long id) {
        return ResponseHelper.okey(penangananService.delete(id));
    }
}
