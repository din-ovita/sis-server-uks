package com.uks.sisserveruks.controller;

import com.uks.sisserveruks.model.Obat;
import com.uks.sisserveruks.response.CommonResponse;
import com.uks.sisserveruks.response.ResponseHelper;
import com.uks.sisserveruks.service.ObatService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

// controller untuk menjalankan sistem method
@RestController
// alamat controller
@RequestMapping("/obat")
public class ObatController {
    @Autowired
    ObatService obatService;

    //    method post
    @PostMapping
    public CommonResponse<Obat> addObat(@RequestBody Obat obat) {
        return ResponseHelper.okey(obatService.addObat(obat));
    }

    //    method get by id
    @GetMapping("/{id}")
    public CommonResponse<Obat> getById(@PathVariable("id") Long id) {
        return ResponseHelper.okey(obatService.getById(id));
    }

    //    method get all
    @GetMapping("/all-obat")
    public CommonResponse<List<Obat>> getAllObat() {
        return ResponseHelper.okey(obatService.getAllObat());
    }

    //    method put
    @PutMapping("/{id}")
    public CommonResponse<Obat> updateObat(@PathVariable("id") Long id, @RequestBody Obat obat) {
        return ResponseHelper.okey(obatService.updateObat(id, obat));
    }

    //    method delete
    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> deleteObat(@PathVariable("id") Long id) {
        return ResponseHelper.okey(obatService.deleteObat(id));
    }
}
