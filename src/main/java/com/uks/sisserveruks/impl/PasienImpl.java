package com.uks.sisserveruks.impl;

import com.uks.sisserveruks.dto.PasienDto;
import com.uks.sisserveruks.dto.StatusPeriksa;
import com.uks.sisserveruks.exception.NotFoundException;
import com.uks.sisserveruks.model.Pasien;
import com.uks.sisserveruks.model.Role;
import com.uks.sisserveruks.repository.*;
import com.uks.sisserveruks.service.PasienService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

// membuat sistem dari method di file service (implementasi method dari file service)
@Service
public class PasienImpl implements PasienService {
    @Autowired
    PasienRepo pasienRepo;

    @Autowired
    DataRepo dataRepo;

    //    sistem post
    @Override
    public Pasien addPasien(PasienDto pasien) {
        Pasien pasien1 = new Pasien();
        pasien1.setData(dataRepo.findById(pasien.getData()).orElseThrow(() -> new NotFoundException("Id not found!")));
        pasien1.setKeluhan(pasien.getKeluhan());
        if (pasien.getStatus().equals("guru")) {
            pasien1.setStatus(Role.guru);
        } else if (pasien.getStatus().equals("siswa")) {
            pasien1.setStatus(Role.siswa);
        } else if (pasien.getStatus().equals("karyawan")) {
            pasien1.setStatus(Role.karyawan);
        }
        pasien1.setPenanganan("Belum ditangani");
        return pasienRepo.save(pasien1);
    }

    //    sistem get by id
    @Override
    public Pasien getById(Long id) {
        return pasienRepo.findById(id).orElseThrow(() -> new NotFoundException("Id not found!"));
    }

    //    sistem update atau put
    @Override
    public Pasien putPasien(PasienDto pasien, Long id) {
        Pasien pasien1 = pasienRepo.findById(id).orElseThrow(() -> new NotFoundException("Id nor found!"));
        pasien1.setKeluhan(pasien.getKeluhan());
        if (pasien.getStatus().equals("guru")) {
            pasien1.setStatus(Role.guru);
        } else if (pasien.getStatus().equals("siswa")) {
            pasien1.setStatus(Role.siswa);
        } else if (pasien.getStatus().equals("karyawan")) {
            pasien1.setStatus(Role.karyawan);
        }
        pasien1.setData(dataRepo.findById(pasien.getData()).orElseThrow(() -> new NotFoundException("Id not found!")));
        return pasienRepo.save(pasien1);
    }

    //    sistem get all
    @Override
    public List<Pasien> allPasien() {
        return pasienRepo.findAll();
    }

    //    sistem delete
    @Override
    public Map<String, Boolean> deletePasien(Long id) {
        try {
            pasienRepo.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Deleted!", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id not found!");
        }
    }

    //    sistem get all by status
    @Override
    public List<Pasien> findStatus(String status) {
        return pasienRepo.findByStatus(Role.valueOf(status));
    }

    //    sistem update atau put penanganan periksa
    @Override
    public Pasien putPeriksa(Long id, StatusPeriksa pasien) {
        Pasien pasien1 = pasienRepo.findById(id).orElseThrow(() -> new NotFoundException("Id not found!"));
        pasien1.setPenanganan("Sudah ditangani");
        return pasienRepo.save(pasien1);
    }

    //    sistem get all by create between
    public Map<String, Object> getBetween(LocalDateTime startDate, LocalDateTime endDate) {
        List<Pasien> datalist = pasienRepo.findByTglPeriksaBetween(startDate, endDate);
        Map<String, Object> response = new HashMap<>();
        response.put("informationUks", datalist);
        return response;
    }
}
