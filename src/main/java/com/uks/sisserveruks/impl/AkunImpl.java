package com.uks.sisserveruks.impl;

import com.uks.sisserveruks.dto.Login;
import com.uks.sisserveruks.exception.InternalErrorException;
import com.uks.sisserveruks.exception.NotFoundException;
import com.uks.sisserveruks.jwt.JwtProvider;
import com.uks.sisserveruks.model.Akun;
import com.uks.sisserveruks.repository.AkunRepo;
import com.uks.sisserveruks.service.AkunService;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

// membuat sistem dari method di file service (implementasi method dari file service)
@Service
public class AkunImpl implements AkunService {
    @Autowired
    AkunRepo akunRepo;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserDetailsService userDetailsService;

//    validasi token dan login
    private String authories(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (BadCredentialsException e) {
            throw new InternalErrorException("Email or Password not found");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        return jwtProvider.generateToken(userDetails);
    }

//    sistem login
    @Override
    public Map<String, Object> login(Login login) {
        String token = authories(login.getEmail(), login.getPassword());
        Akun akuns ;

//        mengecek email
        boolean isEmail = Pattern.compile("^(.+)@(\\S+)$")
                .matcher(login.getEmail()).matches();
        System.out.println("is Email " + isEmail);

//        jika true, akan menjalankan sistem if
        if(isEmail) {
            akuns = akunRepo.findByEmail(login.getEmail());
        } else { // jika false, else akan dijalankan, dgn login username
            akuns = akunRepo.findByUsername(login.getEmail());
        }

        Map<String, Object> response = new HashMap<>();
        response.put("token", token);
        response.put("expired", "15 menit");
        response.put("user", akuns);
        return response;
    }

//    sistem registrasi akun atau method post
    @Override
    public Akun register(Akun akun) {
        String passwordAkun = akun.getPassword().trim();
        boolean PasswordIsNotValid = !passwordAkun.matches("^(?=.*[0-9])(?=.*[a-z]).{8,20}");
        if (PasswordIsNotValid) throw new InternalErrorException ("Password not valid!");
        boolean isEmail = Pattern.compile("^(.+)@(\\S+)$")
                .matcher(akun.getEmail()).matches();
        if (!isEmail) throw new InternalErrorException ("Email not valid!");
        akun.setPassword(passwordEncoder.encode(akun.getPassword()));
        return akunRepo.save(akun);
    }

//    sistem get by id
    @Override
    public Akun getById(Long id) {
        return akunRepo.findById(id).orElseThrow(() -> new NotFoundException("Id not found!"));
    }

//    sistem get all
    @Override
    public List<Akun> getAll() {
        return akunRepo.findAll();
    }

//    sistem update atau put
    @Override
    public Akun update(Long id, Akun akun, MultipartFile multipartFile) {
        Akun akuns = akunRepo.findById(id).orElseThrow(() -> new NotFoundException("Id not found"));
        String url = convertToBase64(multipartFile);
        akuns.setEmail(akun.getEmail());
        akuns.setUsername(akun.getUsername());
        akuns.setFoto(url);
        return akunRepo.save(akuns);
    }

//    sistem update password
    @Override
    public Akun updatePassword(Long id, Akun akun) {
        Akun update = akunRepo.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
        String UserPassword = akun.getPassword().trim();
        boolean PasswordIsNotValid = !UserPassword.matches("^(?=.*[0-9])(?=.*[a-z]).{8,20}");
        if (PasswordIsNotValid) throw new InternalErrorException("Password not valid!");
        update.setPassword(passwordEncoder.encode(akun.getPassword()));
        return akunRepo.save(update);
    }

//    sistem delete
    @Override
    public Map<String, Boolean> deleteAkun(Long id) {
        try {
            akunRepo.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Deleted!", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id not found!");
        }
    }

//    type file
    private String convertToBase64(MultipartFile file) {
        String url = "";
        try {
            byte[] byteData = Base64.encodeBase64(file.getBytes());
            String res = new String(byteData);
            url = "data:" + file.getContentType() + ";base64," + res;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return url;
        }
    }
}
