package com.uks.sisserveruks.service;

import com.uks.sisserveruks.model.Penanganan;

import java.util.List;
import java.util.Map;

// untuk membuat method atau fungsi
public interface PenangananService {
    //    method post
    Penanganan addPenanganan(Penanganan penanganan);

    //    method get by id
    Penanganan getId(Long id);

    //    method get all
    List<Penanganan> getAllPenanganan();

    //    method put
    Penanganan putPenanganan(Long id, Penanganan penanganan);

    //    method delete
    Map<String, Boolean> delete(Long id);

}
