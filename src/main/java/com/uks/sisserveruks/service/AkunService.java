package com.uks.sisserveruks.service;

import com.uks.sisserveruks.dto.Login;
import com.uks.sisserveruks.model.Akun;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

// untuk membuat method atau fungsi
public interface AkunService {
//    method post
    Akun register(Akun akun);

//    method get by id
    Akun getById(Long id);

//    method get all
    List<Akun> getAll();

//    method put
    Akun update(Long id, Akun akun, MultipartFile multipartFile);

//    method delete
    Map<String, Boolean> deleteAkun(Long id);

//    method login
    Map<String, Object> login(Login login);

//    method update password
    Akun updatePassword(Long id, Akun akun);

}
