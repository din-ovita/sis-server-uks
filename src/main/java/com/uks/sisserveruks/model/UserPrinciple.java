package com.uks.sisserveruks.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

// membuat implementasi UserDetails, untuk membuat authentification
public class UserPrinciple implements UserDetails {
//    validasi login
    private String email;

    private String password;

    private Collection<? extends GrantedAuthority> authority;

    public UserPrinciple(String email, String password) {
        this.email = email;
        this.password = password;
        this.authority = authority;
    }

    public static UserPrinciple build(Akun akun) {
        return new UserPrinciple(
                akun.getEmail(),
                akun.getPassword()
        );
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authority;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
