package com.uks.sisserveruks.service;


import com.uks.sisserveruks.model.Data;

import java.util.List;
import java.util.Map;

// untuk membuat method atau fungsi
public interface DataService {
    //    method post
    Data addData(Data data);

    //    method get by id
    Data getById(Long id);

    //    method get all
    List<Data> getAllData();

    //    method put
    Data putData(Long id, Data data);

    //    method delete
    Map<String, Boolean> delete(Long id);

    //    method get all by status data
    List<Data> findStatus(String status);

}
