package com.uks.sisserveruks.controller;

import com.uks.sisserveruks.dto.PasienDto;
import com.uks.sisserveruks.dto.StatusPeriksa;
import com.uks.sisserveruks.model.Pasien;
import com.uks.sisserveruks.response.CommonResponse;
import com.uks.sisserveruks.response.ResponseHelper;
import com.uks.sisserveruks.service.PasienService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

// controller untuk menjalankan sistem method
@RestController
// alamat controller
@RequestMapping("/pasien")
public class PasienController {
    @Autowired
    PasienService pasienService;

    //    method post
    @PostMapping
    public CommonResponse<Pasien> addPasien(@RequestBody PasienDto pasien) {
        return ResponseHelper.okey(pasienService.addPasien(pasien));
    }

    //    method get by id
    @GetMapping("/{id}")
    public CommonResponse<Pasien> getById(@PathVariable("id") Long id) {
        return ResponseHelper.okey(pasienService.getById(id));
    }

    //    method get all
    @GetMapping("/all-pasien")
    public CommonResponse<List<Pasien>> allPasien() {
        return ResponseHelper.okey(pasienService.allPasien());
    }

    //    method put
    @PutMapping("/{id}")
    public CommonResponse<Pasien> putPasien(@RequestBody PasienDto pasien, @PathVariable("id") Long id) {
        return ResponseHelper.okey(pasienService.putPasien(pasien, id));
    }

    //    method put penanganan
    @PutMapping("/periksa/{id}")
    public CommonResponse<Pasien> putPeriksa(@PathVariable("id") Long id, @RequestBody StatusPeriksa pasien) {
        return ResponseHelper.okey(pasienService.putPeriksa(id, pasien));
    }

    //    method delete
    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> deletePasien(@PathVariable("id") Long id) {
        return ResponseHelper.okey(pasienService.deletePasien(id));
    }

    //    method get all by status
    @GetMapping("/all-by-status-pasien")
    public CommonResponse<List<Pasien>> findStatus(@RequestParam(name = "status") String status) {
        return ResponseHelper.okey(pasienService.findStatus(status));
    }

    //    method get all by status
//    @GetMapping("/rekap-data")
//    public CommonResponse<Map<String, Object>> getRekapData(@RequestParam("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date start, @RequestParam("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) {
//        return ResponseHelper.okey(pasienService.getBetween(start, endDate));
//    }
}
