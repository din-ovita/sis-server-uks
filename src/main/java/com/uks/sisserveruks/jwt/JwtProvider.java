package com.uks.sisserveruks.jwt;

import com.uks.sisserveruks.exception.InternalErrorException;
import com.uks.sisserveruks.model.Akun;
import com.uks.sisserveruks.model.TemporaryToken;
import com.uks.sisserveruks.repository.AkunRepo;
import com.uks.sisserveruks.repository.TokenRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public class JwtProvider {
    private static String secretKey = "token";

    private static Integer expired = 600000;

    @Autowired
    private TokenRepo tokenRepo;

    @Autowired
    private AkunRepo akunRepo;

//    membuat token
    public String generateToken(UserDetails userDetails) {
        String token = UUID.randomUUID().toString().replace("-", "");
        Akun akuns = akunRepo.findByEmail(userDetails.getUsername());
        var checkingToken = tokenRepo.findByAkunId(akuns.getId());
        if (checkingToken.isPresent()) tokenRepo.deleteById(checkingToken.get().getId());
        TemporaryToken temporaryToken = new TemporaryToken();
        temporaryToken.setToken(token);
        temporaryToken.setExpiredDate(new Date(new Date().getTime() + expired));
        temporaryToken.setAkunId(akuns.getId());
        tokenRepo.save(temporaryToken);
        return token;
    }

//    get token
    public TemporaryToken getSubject(String token) {
        return tokenRepo.findByToken(token).orElseThrow(() -> new InternalErrorException("Token error!"));
    }

//    mengecek token
    public boolean checkingTokenJwt(String token) {
        TemporaryToken tokenExist = tokenRepo.findByToken(token).orElse(null);
        if (tokenExist == null) {
            System.out.println("Token kosong");
            return false;
        }
        if (tokenExist.getExpiredDate().before(new Date())) {
            System.out.println("Token expired");
            return false;
        }
        return true;
    }

}
