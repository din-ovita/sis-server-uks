package com.uks.sisserveruks.repository;

import com.uks.sisserveruks.model.Akun;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

// membuat repository, untuk membuat query sqlyog
@Repository
public interface AkunRepo extends JpaRepository<Akun, Long> {
//    query by email
    Akun findByEmail(String email);
//    query by username
    Akun findByUsername(String username);
}
