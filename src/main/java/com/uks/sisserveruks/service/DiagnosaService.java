package com.uks.sisserveruks.service;

import com.uks.sisserveruks.model.Diagnosa;

import java.util.List;
import java.util.Map;

// untuk membuat method atau fungsi
public interface DiagnosaService {
    //    method post
    Diagnosa addDiagnosa(Diagnosa diagnosa);

    //    method get by id
    Diagnosa getById(Long id);

    //    method get all
    List<Diagnosa> getAllDiagnosa();

    //    method put
    Diagnosa putDiagnosa(Long id, Diagnosa diagnosa);

    //    method delete
    Map<String, Boolean> delete(Long id);

}
