package com.uks.sisserveruks.controller;

import com.uks.sisserveruks.model.Diagnosa;
import com.uks.sisserveruks.response.CommonResponse;
import com.uks.sisserveruks.response.ResponseHelper;
import com.uks.sisserveruks.service.DiagnosaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

// controller untuk menjalankan sistem method
@RestController
// alamat controller
@RequestMapping("/diagnosa")
public class DiagnosaController {
    @Autowired
    DiagnosaService diagnosaService;

    //    method post
    @PostMapping
    public CommonResponse<Diagnosa> addDiagnosa(@RequestBody Diagnosa diagnosa) {
        return ResponseHelper.okey(diagnosaService.addDiagnosa(diagnosa));
    }

    //    method get by id
    @GetMapping("/{id}")
    public CommonResponse<Diagnosa> getId(@PathVariable("id") Long id) {
        return ResponseHelper.okey(diagnosaService.getById(id));
    }

    //    method get all
    @GetMapping("/all-diagnosa")
    public CommonResponse<List<Diagnosa>> getAllDiagnosa() {
        return ResponseHelper.okey(diagnosaService.getAllDiagnosa());
    }

    //    method put
    @PutMapping("/{id}")
    public CommonResponse<Diagnosa> putDiagnosa(@PathVariable("id") Long id, @RequestBody Diagnosa diagnosa) {
        return ResponseHelper.okey(diagnosaService.putDiagnosa(id, diagnosa));
    }

    //    method delete
    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> delete(@PathVariable("id") Long id) {
        return ResponseHelper.okey(diagnosaService.delete(id));
    }

}
