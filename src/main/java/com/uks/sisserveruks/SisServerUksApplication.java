package com.uks.sisserveruks;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SisServerUksApplication {
//	install model mapper
	@Bean
	public ModelMapper modelMapper() {return new ModelMapper();}

//	menjalankan sistem
	public static void main(String[] args) {
		SpringApplication.run(SisServerUksApplication.class, args);
		System.out.println("SUKSES!!");
	}

}
