package com.uks.sisserveruks.controller;

import com.uks.sisserveruks.excel.ExcelDataHelper;
import com.uks.sisserveruks.excel.ExcelDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;

// controller untuk menjalankan sistem method
@RestController
// alamat controller
@RequestMapping("/data/api/excel")
public class ExcelDataController {
    @Autowired
    ExcelDataService excelDataService;

//    method download data
    @GetMapping("/download/data")
    public ResponseEntity<Resource> getFileData(@RequestParam("status") String status) {
        String filename = "data.xlsx";
        InputStreamResource file = new InputStreamResource(excelDataService.load(status));

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }

    private static final String EXTERNAL_FILE_PATH_DATA = "contoh-format-data.xlsx";

//    download data periksa by id
    @GetMapping("/download/{id}")
    public ResponseEntity<Resource> download(@PathVariable("id") Long id) {
        String filename = "periksa.xlsx";
        InputStreamResource file = new InputStreamResource(excelDataService.download(id));

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }

//    method upload excel to data
    @PostMapping("/upload/data")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
        String message = "";
        if (ExcelDataHelper.hasExcelFormat(file)) {
            try {
                excelDataService.saveData(file);
                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                return ResponseEntity.status(HttpStatus.OK).body(message);
            } catch (Exception e) {
                System.out.println(e);
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
            }
        }
        message = "Please upload an excel file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
    }

//    method download format data
    @GetMapping("/download/format-data")
    public void downloadFormatFile(HttpServletResponse response) throws IOException {
        File file = new File(EXTERNAL_FILE_PATH_DATA);
        if (file.exists()) {
            String mimeType = URLConnection.guessContentTypeFromName(file.getName());
            if (mimeType == null) {
                mimeType = "application/octet-stream";
            }
            response.setContentType(mimeType);
            response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));
            response.setContentLength((int) file.length());

            InputStream inputStream = new BufferedInputStream(new FileInputStream(file));

            FileCopyUtils.copy(inputStream, response.getOutputStream());
        }
    }
}

