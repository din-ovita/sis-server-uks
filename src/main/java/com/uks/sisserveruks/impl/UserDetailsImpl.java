package com.uks.sisserveruks.impl;

import com.uks.sisserveruks.model.Akun;
import com.uks.sisserveruks.model.UserPrinciple;
import com.uks.sisserveruks.repository.AkunRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

// membuat sistem dari method di file service (implementasi method dari file service)
@Service
public class UserDetailsImpl implements UserDetailsService {
    @Autowired
    AkunRepo akunRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        mengecek email
        boolean isEmail = Pattern.compile("^(.+)@(\\S+)$")
                .matcher(username).matches();
        Akun akun;
        System.out.println("Is email " + isEmail);

//        jika true, login menggunakan email
        if (isEmail) {
            akun = akunRepo.findByEmail(username);
        } else {
//            login menggunakan username
            akun = akunRepo.findByUsername(username);
        }
        return UserPrinciple.build(akun);
    }
}
