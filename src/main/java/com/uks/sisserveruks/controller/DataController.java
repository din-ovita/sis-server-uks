package com.uks.sisserveruks.controller;

import com.uks.sisserveruks.model.Data;
import com.uks.sisserveruks.response.CommonResponse;
import com.uks.sisserveruks.response.ResponseHelper;
import com.uks.sisserveruks.service.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

// controller untuk menjalankan sistem method
@RestController
// alamat controller
@RequestMapping("/data")
public class DataController {
    @Autowired
    DataService dataService;

    //    method post
    @PostMapping
    public CommonResponse<Data> addData(@RequestBody Data data) {
        return ResponseHelper.okey(dataService.addData(data));
    }

    //    method get by id
    @GetMapping("/{id}")
    public CommonResponse<Data> getById(@PathVariable("id") Long id) {
        return ResponseHelper.okey(dataService.getById(id));
    }

    //    method put
    @PutMapping("/{id}")
    public CommonResponse<Data> putData(@PathVariable("id") Long id, Data data) {
        return ResponseHelper.okey(dataService.putData(id, data));
    }

    //    method delete
    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> delete(@PathVariable("id") Long id) {
        return ResponseHelper.okey(dataService.delete(id));
    }

    //    method get all
    @GetMapping("/all-data")
    public CommonResponse<List<Data>> getAllData() {
        return ResponseHelper.okey(dataService.getAllData());
    }

    //    method get all by status
    @GetMapping("/all-by-status")
    public CommonResponse<List<Data>> findStatus(@RequestParam(name = "status") String status) {
        return ResponseHelper.okey(dataService.findStatus(status));
    }
}
