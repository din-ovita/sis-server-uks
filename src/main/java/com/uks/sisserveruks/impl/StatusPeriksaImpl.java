package com.uks.sisserveruks.impl;

import com.uks.sisserveruks.dto.PenangananPasien;
import com.uks.sisserveruks.exception.NotFoundException;
import com.uks.sisserveruks.model.StatusPeriksa;
import com.uks.sisserveruks.repository.*;
import com.uks.sisserveruks.service.StatusPeriksaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// membuat sistem dari method di file service (implementasi method dari file service)
@Service
public class StatusPeriksaImpl implements StatusPeriksaService {
    @Autowired
    StatusPeriksaRepo statusPeriksaRepo;

    @Autowired
    PasienRepo pasienRepo;

    @Autowired
    DiagnosaRepo diagnosaRepo;

    @Autowired
    PenangananRepo penangananRepo;

    @Autowired
    TindakanRepo tindakanRepo;

    @Autowired
    ObatRepo obatRepo;

    //    sistem post
    @Override
    public StatusPeriksa add(PenangananPasien penangananPasien) {
        StatusPeriksa statusPeriksa1 = new StatusPeriksa();
        statusPeriksa1.setPasienId(pasienRepo.findById(penangananPasien.getPasienId()).orElseThrow(() -> new NotFoundException("Id not found!")));
        statusPeriksa1.setPenangananId(penangananRepo.findById(penangananPasien.getPenangananId()).orElseThrow(() -> new NotFoundException("Id not found!")));
        statusPeriksa1.setDiagnosaId(diagnosaRepo.findById(penangananPasien.getDiagnosaId()).orElseThrow(() -> new NotFoundException("Id not found!")));
        statusPeriksa1.setTindakanId(tindakanRepo.findById(penangananPasien.getTindakanId()).orElseThrow(() -> new NotFoundException("Id not found!")));
        statusPeriksa1.setObatId(obatRepo.findById(penangananPasien.getObatId()).orElseThrow(() -> new NotFoundException("Id not found!")));
        return statusPeriksaRepo.save(statusPeriksa1);
    }

    //    sistem get by id
    @Override
    public StatusPeriksa getById(Long id) {
        return statusPeriksaRepo.findById(id).orElseThrow(() -> new NotFoundException("Id not found!"));
    }

    //    sistem get all
    @Override
    public List<StatusPeriksa> getAll() {
        return statusPeriksaRepo.findAll();
    }

    //    sistem get all by pasien id
    @Override
    public List<StatusPeriksa> getByPasien(Long pasienId) {
        return statusPeriksaRepo.findByPasien(pasienId);
    }
}
