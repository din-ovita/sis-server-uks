package com.uks.sisserveruks.controller;

import com.uks.sisserveruks.dto.PenangananPasien;
import com.uks.sisserveruks.model.StatusPeriksa;
import com.uks.sisserveruks.response.CommonResponse;
import com.uks.sisserveruks.response.ResponseHelper;
import com.uks.sisserveruks.service.StatusPeriksaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// controller untuk menjalankan sistem method
@RestController
// alamat controller
@RequestMapping("/status-periksa")
public class StatusPeriksaController {
    @Autowired
    StatusPeriksaService statusPeriksaService;

    //    method post
    @PostMapping
    public CommonResponse<StatusPeriksa> add(@RequestBody PenangananPasien penangananPasien) {
        return ResponseHelper.okey(statusPeriksaService.add(penangananPasien));
    }

    //    method get by id
    @GetMapping("/{id}")
    public CommonResponse<StatusPeriksa> getById(@PathVariable("id") Long id) {
        return ResponseHelper.okey(statusPeriksaService.getById(id));
    }

    //    method get all
    @GetMapping
    public CommonResponse<List<StatusPeriksa>> getAll() {
        return ResponseHelper.okey(statusPeriksaService.getAll());
    }

    //    method get all by pasienId
    @GetMapping("/by-pasien")
    public CommonResponse<List<StatusPeriksa>> getByPasien(@RequestParam(name = "pasien_id") Long pasienId) {
        return ResponseHelper.okey(statusPeriksaService.getByPasien(pasienId));
    }

}
