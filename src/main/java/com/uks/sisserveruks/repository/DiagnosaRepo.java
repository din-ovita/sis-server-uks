package com.uks.sisserveruks.repository;

import com.uks.sisserveruks.model.Diagnosa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

// membuat repository, untuk membuat query sqlyog
@Repository
public interface DiagnosaRepo extends JpaRepository<Diagnosa, Long> {
}
