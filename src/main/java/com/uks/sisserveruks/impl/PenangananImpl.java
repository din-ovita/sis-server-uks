package com.uks.sisserveruks.impl;

import com.uks.sisserveruks.exception.NotFoundException;
import com.uks.sisserveruks.model.Penanganan;
import com.uks.sisserveruks.repository.PenangananRepo;
import com.uks.sisserveruks.service.PenangananService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// membuat sistem dari method di file service (implementasi method dari file service)
@Service
public class PenangananImpl implements PenangananService {
    @Autowired
    PenangananRepo penangananRepo;

    //    sistem post
    @Override
    public Penanganan addPenanganan(Penanganan penanganan) {
        return penangananRepo.save(penanganan);
    }

    //    sistem get by id
    @Override
    public Penanganan getId(Long id) {
        return penangananRepo.findById(id).orElseThrow(() -> new NotFoundException("Id not found!"));
    }

    //    sistem get all
    @Override
    public List<Penanganan> getAllPenanganan() {
        return penangananRepo.findAll();
    }

    //    sistem update atau put
    @Override
    public Penanganan putPenanganan(Long id, Penanganan penanganan) {
        Penanganan penanganans = penangananRepo.findById(id).orElseThrow(() -> new NotFoundException("Id not found!"));
        penanganans.setNamaPenanganan(penanganan.getNamaPenanganan());
        return penangananRepo.save(penanganans);
    }

    //    sistem delete
    @Override
    public Map<String, Boolean> delete(Long id) {
        try {
            penangananRepo.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Deleted!", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id not found!");
        }
    }
}
