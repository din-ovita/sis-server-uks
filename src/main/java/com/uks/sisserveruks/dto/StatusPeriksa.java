package com.uks.sisserveruks.dto;

//membuat dto status periksa
public class StatusPeriksa {
    private String penanganan;

    public String getPenanganan() {
        return penanganan;
    }

    public void setPenanganan(String penanganan) {
        this.penanganan = penanganan;
    }
}
