package com.uks.sisserveruks.repository;

import com.uks.sisserveruks.model.Pasien;
import com.uks.sisserveruks.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

// membuat repository, untuk membuat query sqlyog
@Repository
public interface PasienRepo extends JpaRepository<Pasien, Long> {
//        query get all by status
        List<Pasien> findByStatus(Role status);

//        query get all
        List<Pasien> findByTglPeriksaBetween(LocalDateTime startDate, LocalDateTime endDate);
}
